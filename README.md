# Chuck Norris Joke App
A simple iOS app that interacts with the Internet Chuck Norris Database API

## Technologies

Project is created with:

- Swift 5.2.4


## Setup

To run this project,

You can either clone it via HTTPS or SSH. If you chose to clone it via HTTPS, 
you’ll have to enter your credentials every time you pull and push. 
 With SSH, you enter your credentials only once.

You can find both paths (HTTPS and SSH) by navigating to your project’s landing page and clicking Clone. GitLab will prompt you with both paths, from which you can copy and paste in your command line.

**Find more information with this link**: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html 

## How to build project

- Start a new project in XCode 
- Select the Single View App
- Once selected, type in the project name for example: Joke App
- The language should be swift, the bottom three info should not be ticked (not necessary). Click next to create the project.
- Next go to the Main.storyboard that is where you will start building the interface.
- To add buttons in the view controller at the top corner in XCode where it has a plus button click on it, and you will see a button just drag and drop it on the iPhone view controller. Add in another two buttons, we should have three buttons one for random joke, the other for text input and never-ending joke list.
- Then we need to add a switch to detect weather we want out joke to be explicit or non-explicit, the same way to add a button is the same to add a switch.
- Now we need to make another view controller for the text input, this way when the user clicks on the text input button on the home page it will take them to the new view controller.
- In the text input view controller, we need to add two text fields to type the first name and last name. We also want to add a button to search for jokes. This way we can search for jokes in the app.
- We then have to build another view controller however it will be a table view controller, so to add it into the Main.storyboard we will click on the plus button at the top corner in XCode and search table view controller and drag and drop it.
- Finally, to finish of the app we will need to add a navigation. This way it will be easier to navigate to the other view controllers. To add it to the project you will click on an arrow that’s in a box at the bottom on the Main.Storyborad. 
- That's how to build the application 

