//
//  FetchNoExplictRandomJokes.swift
//  TechTest
//
//  Created by Femi Oladiji on 29/06/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation

var explicit = "[explicit]"
// MARK: - No Explict Jokes Fetch request

class RandomJokesNoExplist {
    
    var randomJokeID: AnyObject?
    var ramdomJoke: AnyObject?
    var randomJokesCategory: AnyObject?
    
    
    
    
    func fetchNoExplistJokes(completion: @escaping () -> Void) {
        
        var urlComponents = URLComponents()
        
        
        urlComponents.scheme = "https"
        urlComponents.host  = "api.icndb.com"
        urlComponents.path = "/jokes/random"
        urlComponents.queryItems = [URLQueryItem(name: "exclude", value: explicit)]
        
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        
    
        
        let dataTask = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error.localizedDescription)")
            } else if let data = data{
                self.parseJSON(data)
                completion()
            }else{
                print("This should never happen")
            }
        }
        dataTask.resume()
    }
    
    func parseJSON(_ data : Data) {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(RootSearchResult.self, from: data)
            randomJokeID = decodedData.value.id as AnyObject
            ramdomJoke = decodedData.value.joke as AnyObject
            randomJokesCategory = decodedData.value.categories as AnyObject
            
        }
        catch {
            print("this is the pass error\(error)")
        }
    }
}

class TextInputNoExplictJokes {
    
    var textInputJokeId: AnyObject?
    var textInputJokes: AnyObject?
    var textInputJokesCategory: AnyObject?
    
    func fetchTextInputNoExplict(_ firstName: String, _ lastName: String, completion: @escaping () -> Void) {
        
        var urlComponents = URLComponents()
        
        
        
        urlComponents.scheme = "https"
        urlComponents.host  = "api.icndb.com"
        urlComponents.path = "/jokes/random"
        urlComponents.queryItems = [URLQueryItem(name: "firstName", value: firstName),
                                    URLQueryItem(name: "&lastName", value: lastName),
                                    URLQueryItem(name: "&exclude", value: explicit)]
        
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        

        
        let dataTask = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error.localizedDescription)")
            } else if let data = data{
                self.parseJSON(data)
                completion()
            }else{
                print("This should never happen")
            }
        }
        dataTask.resume()
    }
    
    func parseJSON(_ data : Data) {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(RootSearchResult.self, from: data)
            textInputJokeId = decodedData.value.id as AnyObject
            textInputJokes = decodedData.value.joke as AnyObject
            textInputJokesCategory = decodedData.value.categories as AnyObject
            
        }
        catch {
            print("this is the pass error\(error)")
        }
    }
    
}



@available(iOS 13.0, *)
class ListOfJokesNoExplict {
    
    var neverEndingNoExplictJokes = [myValues]()
    
    var numberOfNoExplictJokes = 10
    
    func fetchListOfJokesNoExplict(completion: @escaping () -> Void) {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host  = "api.icndb.com"
        urlComponents.path = "/jokes/random/\(numberOfNoExplictJokes)"
        urlComponents.queryItems = [URLQueryItem(name: "exclude", value: explicit)]
        
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        
        let dataTask = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error.localizedDescription)")
            } else if let data = data{
                self.parseJSON(data)
                completion()
            }else{
                print("This should never happen")
            }
        }
        dataTask.resume()
    }
    
    // Decoding the data
    @available(iOS 13.0, *)
    func parseJSON(_ data : Data) {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(SearchResult.self, from: data)
            self.neverEndingNoExplictJokes = decodedData.value
            numberOfNoExplictJokes += 10
        }
        catch {
            print("this is the pass error\(error)")
        }
    }
}

@available(iOS 13.0, *)
extension ListOfJokesNoExplict{
    
    
    var sections: Int{
        return 1
    }
    // Counts the number of rows
    func numberOfRowsInSection(_ section: Int) -> Int {
        return neverEndingNoExplictJokes.count
    }
    
    //Shows all the result in the sections
    func allNoJokes(at indexPath: IndexPath) -> myValues? {
        if indexPath.section < 0 || indexPath.section > sections{
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection(indexPath.section){
            return nil
        }
        return neverEndingNoExplictJokes[indexPath.row]
    }
}
