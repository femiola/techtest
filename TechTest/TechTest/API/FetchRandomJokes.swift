//
//  FetchRandomJokes.swift
//  TechTest
//
//  Created by Femi Oladiji on 25/06/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation


@available(iOS 13.0, *)
class RandomJokes {
    
    var randomJokeID: AnyObject?
    var randomJoke: AnyObject?
    var randomJokeCategory: AnyObject?
    
    func fetchRandomJokes(completion: @escaping () -> Void) {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host  = "api.icndb.com"
        urlComponents.path = "/jokes/random"
        
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        
        let dataTask = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error.localizedDescription)")
            } else if let data = data{
                self.parseJSON(data)
                completion()
            }else{
                print("This should never happen")
            }
        }
        dataTask.resume()
    }
    
    // Decoding the data
    @available(iOS 13.0, *)
    func parseJSON(_ data : Data) {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(RootSearchResult.self, from: data)
            randomJokeID = decodedData.value.id as AnyObject
            randomJoke = decodedData.value.joke as AnyObject
            randomJokeCategory = decodedData.value.categories as AnyObject
            
        }
        catch {
            print("this is the pass error\(error)")
        }
    }
}

class TextInputJokes {
    
    var textInputJokeId: AnyObject?
    var textInputJoke: AnyObject?
    var textInputCategory: AnyObject?
    
    func fetchTextInput(_ firstName: String, _ lastName: String, completion: @escaping () -> Void) {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host  = "api.icndb.com"
        urlComponents.path = "/jokes/random"
        urlComponents.queryItems = [URLQueryItem(name: "firstName", value: firstName),
                                    URLQueryItem(name: "&amp;lastName", value: lastName)]
        
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        
        
        let dataTask = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error.localizedDescription)")
            } else if let data = data{
                self.parseJSON(data)
                completion()
            }else{
                print("This should never happen")
            }
        }
        dataTask.resume()
    }
    
    func parseJSON(_ data : Data) {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(RootSearchResult.self, from: data)
            textInputJokeId = decodedData.value.id as AnyObject
            textInputJoke = decodedData.value.joke as AnyObject
            textInputCategory = decodedData.value.categories as AnyObject
            
        }
        catch {
            print("this is the pass error\(error)")
        }
    }
}


@available(iOS 13.0, *)
class ListOfJokes {
    
    var neverEndingJokes = [myValues]()
    
    var numberOfJokes = 10
    
    func fetchListOfJokes(completion: @escaping () -> Void) {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host  = "api.icndb.com"
        urlComponents.path = "/jokes/random/\(numberOfJokes)"
        
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        
        let dataTask = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error.localizedDescription)")
            } else if let data = data{
                self.parseJSON(data)
                completion()
            }else{
                print("This should never happen")
            }
        }
        dataTask.resume()
    }
    
    // Decoding the data
    @available(iOS 13.0, *)
    func parseJSON(_ data : Data) {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(SearchResult.self, from: data)
            self.neverEndingJokes = decodedData.value
            numberOfJokes += 10
        }
        catch {
            print("this is the pass error\(error)")
        }
    }
}

@available(iOS 13.0, *)
extension ListOfJokes{
    
    
    var sections: Int{
        return 1
    }
    // Counts the number of rows
    func numberOfRowsInSection(_ section: Int) -> Int {
        return neverEndingJokes.count
    }
    
    //Shows all the result in the sections
    func allJokes(at indexPath: IndexPath) -> myValues? {
        if indexPath.section < 0 || indexPath.section > sections{
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection(indexPath.section){
            return nil
        }
        return neverEndingJokes[indexPath.row]
    }
}






