//
//  Alert.swift
//  TechTest
//
//  Created by Femi Oladiji on 29/06/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
struct Alert {
    
    
    static func showRandomJokesPopUp(on vc: UIViewController, with title:String , message: String){
        
        let alert = UIAlertController(title: title , message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK that was funny", style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    

    
    
    
}
