//
//  ListOfJokesTableViewController.swift
//  TechTest
//
//  Created by Femi Oladiji on 26/06/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ListOfJokesTableViewController: UITableViewController {
    
    
    var neverEndingJokesModel: ListOfJokes!
    var neverEndingJokesNoExplict : ListOfJokesNoExplict!
    
    let defaults = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        neverEndingJokesModel = ListOfJokes()
        neverEndingJokesNoExplict = ListOfJokesNoExplict()
        
        showAllTheJokes()
        
    }
    
    
    
    func showAllTheJokes() {
        
        if defaults.value(forKey: "switchON") != nil{
            
            let switchON: Bool = defaults.value(forKey: "switchON")  as! Bool
            
            if switchON == true{
                
                neverEndingJokesNoExplict.fetchListOfJokesNoExplict(completion: {
                    
                    DispatchQueue.main.async {
                        
                        self.tableView.reloadData()
                    }
                })
            }
            else if switchON == false{
                neverEndingJokesModel.fetchListOfJokes(completion:
                    {
                        DispatchQueue.main.async {
                            
                            self.tableView.reloadData()
                        }
                })
            }
        }
    }
    
    func showActivityIndicator() {
    
        let spinner = UIActivityIndicatorView()
        
        spinner.color = .red
        spinner.startAnimating()

        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        self.tableView.tableFooterView = spinner
        
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        if defaults.value(forKey: "switchON") != nil{
            
            let switchON: Bool = defaults.value(forKey: "switchON")  as! Bool
            if switchON == true{
                return neverEndingJokesNoExplict.sections
            }
        }
        return neverEndingJokesModel.sections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if defaults.value(forKey: "switchON") != nil{
            let switchON: Bool = defaults.value(forKey: "switchON")  as! Bool
            if switchON == true{
                return neverEndingJokesNoExplict.numberOfRowsInSection(section)
            }
        }
        
        return neverEndingJokesModel.numberOfRowsInSection(section)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listOfJokes", for: indexPath)
        
        if defaults.value(forKey: "switchON") != nil{
            
            let switchON: Bool = defaults.value(forKey: "switchON")  as! Bool
            if switchON == true {
                if let allTheNerdyJokes = neverEndingJokesNoExplict.allNoJokes(at: indexPath) {
                    
                    cell.textLabel?.text = allTheNerdyJokes.joke
                    cell.detailTextLabel?.text = "\(allTheNerdyJokes.categories)"
                }
            }else if switchON == false {
                
                if let allTheBestJokes = neverEndingJokesModel.allJokes(at: indexPath) {
                    cell.textLabel?.text = allTheBestJokes.joke
                    cell.detailTextLabel?.text = "\(allTheBestJokes.categories)"
                }
            }
        }
        return cell
    }
    
    
    
 override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    
    let indexPath = NSIndexPath(row: neverEndingJokesModel.neverEndingJokes.count - 1, section: 0)
    self.tableView.scrollToRow(at: indexPath as IndexPath, at: .none, animated: true)
 }
 
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if defaults.value(forKey: "switchON") != nil{
            
            let switchON: Bool = defaults.value(forKey: "switchON")  as! Bool
            if switchON == true {
                let jokes = neverEndingJokesNoExplict.neverEndingNoExplictJokes.count - 1
                if indexPath.row == jokes{
                    loadMoreData()
                }
            }
        }
        
        let lastItem = neverEndingJokesModel.neverEndingJokes.count - 1
        if indexPath.row == lastItem {
            loadMoreData()
        }
    }
    
    
    func loadMoreData() {
        if defaults.value(forKey: "switchON") != nil{
            let switchON: Bool = defaults.value(forKey: "switchON")  as! Bool
            if switchON == true{
                
                if neverEndingJokesNoExplict.neverEndingNoExplictJokes.count >= 10{
                    neverEndingJokesNoExplict.fetchListOfJokesNoExplict {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.showActivityIndicator()
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
        
        if neverEndingJokesModel.neverEndingJokes.count >= 10  {
            
            neverEndingJokesModel.fetchListOfJokes {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.showActivityIndicator()
                    self.tableView.reloadData()
                }
            }
        }
    }
}







