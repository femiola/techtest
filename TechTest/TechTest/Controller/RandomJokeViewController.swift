//
//  ViewController.swift
//  TechTest
//
//  Created by Femi Oladiji on 25/06/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit



@available(iOS 13.0, *)
class ViewController: UIViewController {
    
    @IBOutlet weak var mySwitch: UISwitch!
    

    var randomJokesModel: RandomJokes!
    var randomJokeNoExplict: RandomJokesNoExplist!
    
    let defaults = UserDefaults.standard
    var switchON : Bool = false
    
    
    
    
    override func viewDidLoad() {
        randomJokesModel = RandomJokes()
        randomJokeNoExplict = RandomJokesNoExplist()

        super.viewDidLoad()
    }
    
    
    @IBAction func NewEndingJokes(_ sender: UIButton) {
        performSegue(withIdentifier: "neverEndingJokes", sender: self)
        
        if mySwitch.isOn{
            switchON = true
            defaults.set(switchON, forKey: "switchON")
        }
        if mySwitch.isOn == false{
            switchON = false
            defaults.set(switchON, forKey: "switchON")
        }
        
    }
    
    
    @IBAction func TextInput(_ sender: UIButton) {
        
        performSegue(withIdentifier: "textInput", sender: self)
        
        if mySwitch.isOn{
            switchON = true
            defaults.set(switchON, forKey: "switchON")
        }
        if mySwitch.isOn == false{
            switchON = false
            defaults.set(switchON, forKey: "switchON")
        }
    }
    
    
    
    
    @IBAction func ShowRamdomJokes(_ sender: UIButton) {
        
        
        if (mySwitch.isOn == true) {
            randomJokeNoExplict.fetchNoExplistJokes {
                DispatchQueue.main.async {
                    Alert.showRandomJokesPopUp(on: self, with: "Joke ID \(self.randomJokeNoExplict.randomJokeID ?? "" as AnyObject)", message: "\(self.randomJokeNoExplict.ramdomJoke ?? "" as AnyObject) [ \(self.randomJokeNoExplict.randomJokesCategory ?? "" as AnyObject)]")
                }
            }
        }
        else  {
            randomJokesModel.fetchRandomJokes {
                DispatchQueue.main.async {
                    Alert.showRandomJokesPopUp(on: self, with: "Joke ID \(self.randomJokesModel.randomJokeID ?? "" as AnyObject)", message: "\(self.randomJokesModel.randomJoke ?? "" as AnyObject), \(self.randomJokesModel.randomJokeCategory ?? "" as AnyObject)")
                }
            }
        }
    }
}




