//
//  TextInputViewController.swift
//  TechTest
//
//  Created by Femi Oladiji on 26/06/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class TextInputViewController: UIViewController   {
    
    @IBOutlet weak var firstnameTextField: UITextField!
    
    @IBOutlet weak var lastnameTextField: UITextField!
    
    let defaults = UserDefaults.standard
    
    var textInputModel : TextInputJokes!
    var textInputNoExplict : TextInputNoExplictJokes!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        textInputModel = TextInputJokes()
        textInputNoExplict = TextInputNoExplictJokes()
        
    }
    
    
    
    @IBAction func searchJokes(_ sender: Any) {
        
        if defaults.value(forKey: "switchON") != nil{
            
            let switchON: Bool = defaults.value(forKey: "switchON")  as! Bool
            
            if switchON == true{
                textInputNoExplict.fetchTextInputNoExplict(firstnameTextField.text ?? "", lastnameTextField.text ?? "") {
                    
                    DispatchQueue.main.async {
                        
                        Alert.showRandomJokesPopUp(on: self, with: "Joke ID \(self.textInputNoExplict.textInputJokeId ?? "" as AnyObject) ", message: "\(self.textInputNoExplict.textInputJokes ?? "" as AnyObject), [\(self.textInputNoExplict.textInputJokesCategory ?? "" as AnyObject)]")
                        
                    }
                }
            }
            else if switchON == false{
                
                textInputModel.fetchTextInput(firstnameTextField.text ?? "", lastnameTextField.text ?? "") {
                    DispatchQueue.main.async {
                        
                        Alert.showRandomJokesPopUp(on: self, with: "Joke ID \(self.textInputModel.textInputJokeId ?? "" as AnyObject) ", message: "\(self.textInputModel.textInputJoke ?? "" as AnyObject) \( self.textInputModel.textInputCategory ?? "" as AnyObject))")
                        
                    }
                }
            }
        }
    }
}



