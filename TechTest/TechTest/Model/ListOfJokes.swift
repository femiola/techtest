//
//  ListOfJokes.swift
//  TechTest
//
//  Created by Femi Oladiji on 26/06/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation


struct SearchResult: Codable {
    let value: [myValues]
    
    enum CodingKeys: String, CodingKey {
        case  value
    }
}

struct myValues: Codable {
    let id: Int
    let joke: String
    let categories: [String]
    
   enum CodingKeys: String, CodingKey {
       case id, joke, categories
   }
}
