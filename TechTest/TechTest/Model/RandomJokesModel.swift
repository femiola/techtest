//
//  RandomJokesModel.swift
//  TechTest
//
//  Created by Femi Oladiji on 25/06/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation

struct RootSearchResult: Codable {
    let value: Values
    
    enum CodingKeys: String, CodingKey {
        case  value
    }
}

struct Values: Codable {
    let id: Int
    let joke: String
    let categories: [String]
    
    
   enum CodingKeys: String, CodingKey {
       case id, joke, categories
   }
}


